<?php
class inmueblesClass extends validate
{
    public function getInmuebleById($id){
        $this->url.='&action=getInmuebleById&detail='.$id;
        $rtaJSON = @file_get_contents($this->url);
        $result=$this->processDataToResult($rtaJSON);
        return $result;
    }
    public function getIdInmuebleByMatricula($matricula){
        $this->url.='&action=getIdInmuebleByMatricula&detail='.$matricula;
        $rtaJSON=@file_get_contents($this->url);
        $result=$this->processDataToResult($rtaJSON);
        return $result;
    }
    public function newInmueble($arrayParams){
        $this->url.='&action=newInmueble';
        $data = http_build_query($arrayParams);
        $ch = curl_init ($this->url); // your URL to send array data
        curl_setopt ($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // Your array field
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_FAILONERROR,true);
        $rtaJSON = curl_exec ($ch);
        $result=$this->processDataToResult($rtaJSON);
        curl_close($ch);
        return $result;
    }
    public function updateInmuebleById($arrayParams){
        $this->url.='&action=updateInmuebleById';
        $data = http_build_query($arrayParams);
        $ch = curl_init ($this->url); // your URL to send array data
        curl_setopt ($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // Your array field
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $rtaJSON = curl_exec ($ch);
        $result=$this->processDataToResult($rtaJSON);
        curl_close($ch);
        return $result;
    }
}