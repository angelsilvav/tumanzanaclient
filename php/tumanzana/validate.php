<?php
class validate
{
    protected $key=array();
    protected $url='http://admin.tumanzana.com/prueba/API/inmuebles.php';
    public function __construct($mail, $pass)
    {
        $this->key['correo']=$mail;
        $this->key['key']=$pass;
        $keyJSON=json_encode($this->key);
        $this->url.='?key='.$keyJSON;
    }
    public function processDataToResult($data)
    {
        if($data===FALSE||!isset($data)||$data==''){
            $result=json_encode(array('process'=>404,'detail'=>'No se ha establecido conexión con el WebService.'));
        }else{
            $result=$data;
        }
        $process=json_decode(utf8_encode($result),true); // to array php
        //$process=$data;  // to array JSON
        return $process;
    }
}