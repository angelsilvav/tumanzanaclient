<?php
require('validate.php');
require('inmueblesClass.php');
class tumanzanaClient extends validate
{
    public function getInmuebleById($id){
        $class=new inmueblesClass($this->key['correo'],$this->key['key']);
        $inmueble=$class->getInmuebleById($id);
        return $inmueble;
    }
    public function getIdInmuebleByMatricula($matricula){
        $class=new inmueblesClass($this->key['correo'],$this->key['key']);
        $id=$class->getIdInmuebleByMatricula($matricula);
        return $id;
    }
    public function newInmueble($arrayParams){
        $class=new inmueblesClass($this->key['correo'],$this->key['key']);
        $process=$class->newInmueble($arrayParams);
        return $process;
    }
    public function updateInmuebleById($arrayParams){
        $class=new inmueblesClass($this->key['correo'],$this->key['key']);
        $process=$class->updateInmuebleById($arrayParams);
        return $process;
    }

}