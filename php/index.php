<?php
// consumir servicio REST por cliente PHP:
// incluir clases necesarias:
require 'tumanzana/tumanzana.php';
// instanciar clase principal, enviar como parámetro los datos de acceso
$tumanzana=new tumanzanaClient('correo','key');


// PRUEBA DE LOS SERVICIOS

/**
 * solicitud de la info de un inmueble, buscando por ID
 * @param int
 * @return array
 * returns:
 * array['process'] = bool con estado del proceso: 1 realizado || 0 no realizado verificar detalle en
 * array['detail'] = array con info del inmueble encontrado || string cuando no existe id
 */


$id='12';
$arrayInmueble=$tumanzana->getInmuebleById($id);
print_r($arrayInmueble);




/**
 * solicitud del ID de un inmueble, buscando por MATRICULA
 * @param string
 * @return array
 * returns:
 * array['process'] = bool con estado del proceso: 1 realizado || 0 no realizado verificar detalle en
 * array['detail'] = array con id del inmueble encontrado || string cuando no existe la matrícula
 */

/*
$matricula='50C-1603597';
$idInmueble=$tumanzana->getIdInmuebleByMatricula($matricula);
print_r($idInmueble);
*/



/**
 * creación de un inmueble nuevo
 * @param array
 * @return array
 * returns:
 * array['process'] = bool con estado del proceso: 1 realizado || 0 no realizado verificar detalle en
 * array['detail'] = string con detalle del proceso
 */

/*
// crear el array con los datos del inmueble - revisar diccionario de datos con posibles valores y tipos
$arrayParams=array(
    'matricula'=>'miMatricasas2',        // debe ser único, validar con la función getIdInmuebleByMatricula
    'estrato'=>'9',
    'nombreconjunto'=>'mi conjunto',
    'direccion'=>'mi direccion',
    'barrio'=>'mi barrio',

    'fkCiudadId'=>'1',
    'fkLocalidadId'=>'2',
    'fkMunicipioId'=>'3',
    'fkDepartamentoId'=>'4',
    'fkPaisId'=>'1',
    'latitud'=>'123123',
    'longitud'=>'234234',
    'precioventa'=>'9090',

    'precioarriendo'=>'1010',
    'preciodia'=>'5050',
    'precioadmon'=>'2020',
    'valorm2arriendo'=>'1212',
    'valorm2venta'=>'1313',
    'areaconstruida'=>'4040',
    'areaprivada'=>'8080',
    'arealote'=>'55',
    'propiedadhorizontal'=>'1',
    'fkTipoinmuebleId'=>'5',

    'fkTipoofertaId'=>'6',
    'fkEstadoinmuebleId'=>'2',
    'anioconstruido'=>'2099',
    'pisoNumero'=>'666',
    'banios'=>'999',
    'dormitorios'=>'888',
    'parqueadero'=>'777',
    'bodega'=>'555',
    'terraza'=>'444',

    'chimenea'=>'333',
    'garaje'=>'222',
    'balcon'=>'111',
    'estudio'=>'2',
    'comentarios'=>'mis comments',
    'jardin'=>'4',
    'amoblado'=>'5',
    'piscina'=>'6',
    'calefaccion'=>'7',
    'azotea'=>'8',

    'aireacondicionado'=>'9',
    'equipamientoconjunto'=>'si tiene equipment',
    'totalunidadesconstruidas'=>'91',
    'fkAgenteId'=>'2',
    'fkSucursalId'=>'3',
    'fkInmobiliariaId'=>'4',
    'fkUsuarioId'=>'5',
    'activo'=>'1',
    'tum'=>'1',

    'mls'=>'1',
    'comisionTotal'=>'50',
    'comisionCompartida'=>'40',
    'comisionCompartidaArr'=>'30',
    'comisionMeses'=>'20'
);

$resultado=$tumanzana->newInmueble($arrayParams);
print_r($resultado);
*/



/**
 * actualización de un inmueble existente
 * @param array
 * @return array
 * returns:
 * array['process'] = bool con estado del proceso: 1 realizado || 0 no realizado verificar detalle en
 * array['detail'] = string con detalle del proceso
 */

/*
// crear el array con los datos del inmueble - revisar diccionario de datos con posibles valores y tipos
$arrayParams=array(
    'id'=>'2',              // puede ser obtenido con la Función getIdInmuebleByMatricula

    'matricula'=>'miMatricads',        // debe ser único, validar con la función getIdInmuebleByMatricula
    'estrato'=>'9',
    'nombreconjunto'=>'mi conjunto',
    'direccion'=>'mi direccion',
    'barrio'=>'mi barrio',

    'fkCiudadId'=>'1',
    'fkLocalidadId'=>'2',
    'fkMunicipioId'=>'3',
    'fkDepartamentoId'=>'4',
    'fkPaisId'=>'1',
    'latitud'=>'123123',
    'longitud'=>'234234',
    'precioventa'=>'9090',

    'precioarriendo'=>'1010',
    'preciodia'=>'5050',
    'precioadmon'=>'2020',
    'valorm2arriendo'=>'1212',
    'valorm2venta'=>'1313',
    'areaconstruida'=>'4040',
    'areaprivada'=>'8080',
    'arealote'=>'55',
    'propiedadhorizontal'=>'1',
    'fkTipoinmuebleId'=>'5',

    'fkTipoofertaId'=>'6',
    'fkEstadoinmuebleId'=>'2',
    'anioconstruido'=>'2099',
    'pisoNumero'=>'666',
    'banios'=>'999',
    'dormitorios'=>'888',
    'parqueadero'=>'777',
    'bodega'=>'555',
    'terraza'=>'444',

    'chimenea'=>'333',
    'garaje'=>'222',
    'balcon'=>'111',
    'estudio'=>'2',
    'comentarios'=>'mis comments',
    'jardin'=>'4',
    'amoblado'=>'5',
    'piscina'=>'6',
    'calefaccion'=>'7',
    'azotea'=>'8',

    'aireacondicionado'=>'9',
    'equipamientoconjunto'=>'si tiene equipment',
    'totalunidadesconstruidas'=>'91',
    'fkAgenteId'=>'2',
    'fkSucursalId'=>'3',
    'fkInmobiliariaId'=>'4',
    'fkUsuarioId'=>'5',
    'activo'=>'1',
    'tum'=>'1',

    'mls'=>'1',
    'comisionTotal'=>'50',
    'comisionCompartida'=>'40',
    'comisionCompartidaArr'=>'30',
    'comisionMeses'=>'20'
);

$resultado=$tumanzana->updateInmuebleById($arrayParams);
print_r($resultado);
*/